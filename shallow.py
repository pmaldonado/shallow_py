'''
SHALLOW WATER UNIDIMENSIONAL MODEL

Equations:
 1) Ut = -U*Ux - gHx + fV
 2) Vt = -UVx - fU
 3) Ht = -(UH)x

Boundary condition types:
 1) Cyclic
 2) Specified (from an existing .npz file)
 3) Rigid (U=0 in the boundary)
 4) Anti-simetric
 5) Fixed

Initial condition types:
 1) Sinusoidal wave
 2) Centered step function
 3) Gaussian function

Space integration methods:
 1) Centered (2nd order)
 2) Centered (6th order)

Time integration methods:
 1) Euler
 2) Leapfrog
 3) Runge Kutta (3rd order)
 4) Leapfrog: ec. linealizadas
 5) Leapfrog: ec. flujo (solo bordes ciclicos)
 6) Leapfrog: ec. flujo + ret. staggerd (solo bordes ciclicos)
 7) Leapfrog: bordes abiertos
 8) Leapfrog: timesplit
'''
import sys
import os
import math
import numpy as np
from scipy.fftpack import fft
import matplotlib.pyplot as plt
import bc
import methods as met

#### SET UP PARAMETERS AND VARIABLES ####
outdir = './exp6c'
fileout = 'shallow.npz'
filebc = './bc/bc.npz' #bc.npz o bc_interp.npz
if not os.path.exists(outdir):
    os.makedirs(outdir)

nx = 500  #Number of points in the x-direction
nt = 2000 #Number of times to integrate the model
dx = 500  #Space between nodes (in meters)
dt = 5   #Time interval between integrations (in seconds)

bc_type = 3      #Boundary condition type
ic_type = 3      #Initial condition type
gwidth = 10000   #Ancho de la campana de Gauss en m
space_method = 1 #Space integration method
time_method = 7  #Time integration method

f = -7.29e-5         #Coriolis parameter
g = 9.8       #Gravitiy
hs = 0.0      #Bottom height (not used for now)
#------------------------------------------------------------------------------#
#### ALLOCATE VARIABLES ####
u = np.empty((nt, nx))*np.nan
v = np.empty((nt, nx))*np.nan
h = np.empty((nt, nx))*np.nan
KE = []
PE = []
M = []

#### INITIAL AND (INITIAL) BOUNDARY CONDITIONS####
#Set up initial conditions
if bc_type == 2:
    bc = np.load(filebc)
    boff = 143 #Number of points distance between the global and the regional domains
    ub = bc['ub']
    vb = bc['vb']
    hb = bc['hb']
    uini = bc['uini'][boff:boff+nx]
    vini = bc['vini'][boff:boff+nx]
    hini = bc['hini'][boff:boff+nx]
else:
    boff = np.nan
    ub = np.empty((nt, 1))*np.nan
    vb = np.empty((nt, 1))*np.nan
    hb = np.empty((nt, 1))*np.nan

    x = np.arange(0, nx)*dx
    uini = np.zeros(nx)
    vini = np.zeros(nx)

    if ic_type == 1:
        hini = 50 + 5*np.sin(x*2*np.pi/((nx-2)*dx))
    elif ic_type == 2:
        hini = 50*np.ones(nx)
        hini[math.ceil(nx/2)-50:math.ceil(nx/2)+50] = 52
    elif ic_type == 3:
        hini = 50 + 5*np.exp(-((x-np.mean(x)-50000)**2)/(gwidth**2))
    else:
        sys.exit('Not a valid initial_condition option')

#Set up boundary conditions
u0 = uini
v0 = vini
h0 = hini

u0, v0, h0 = bc.boundary(u0, v0, h0, bc_type, ub[0, :], vb[0, :], hb[0, :], boff, dt)

#Save initial time to output variables
u[0, :] = u0
v[0, :] = v0
h[0, :] = h0

#### BEGIN INTEGRATION ####
#First step: EULER
if time_method != 4:
    u1, v1, h1 = met.euler(u0, v0, h0, dx, dt, space_method, f, g)
    u1, v1, h1 = bc.boundary(u1, v1, h1, bc_type, ub[0, :], vb[0, :], hb[0, :], boff, dt)
else:
    u1, v1, h1 = met.euler_lineal(u0, v0, h0, dx, dt, space_method, f, g)
    u1, v1, h1 = bc.boundary(u1, v1, h1, bc_type, ub[0, :], vb[0, :], hb[0, :], boff, dt)

#Save time step to output variables
u[1, :] = u1
v[1, :] = v1
h[1, :] = h1

#Rest of integration steps
for i in range(2, nt):
    if time_method == 1:
        ### EULER ###
        unext, vnext, hnext = met.euler(u1, v1, h1, dx, dt, space_method, f, g)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)
    if time_method == 2:
        ### LEAPFROG ###
        unext, vnext, hnext = met.leapfrog(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, filtercm=True, it=i, filterit=100)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)
    if time_method == 3:
        ### RK 3 ###
        ubrk = 0.5*(ub[i-1, :]+ub[i, :])
        vbrk = 0.5*(vb[i-1, :]+vb[i, :])
        hbrk = 0.5*(hb[i-1, :]+hb[i, :])
        unext, vnext, hnext = met.rk3(u1, v1, h1, dx, dt, space_method, f, g, bc_type, ub, vb, hb, boff, it=i)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)
    if time_method == 4:
        ### LEAPFROG LINEAL ###
        unext, vnext, hnext = met.leapfrog_lineal(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, filtercm=True, it=i, filterit=100)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)
    if time_method == 5:
        ### LEAPFROG EULER ###
        unext, vnext, hnext = met.leapfrog_euler(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, filtercm=True, it=i, filterit=100)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)
    if time_method == 6:
        ### LEAPFROG EULER STAGGERED ###
        unext, vnext, hnext = met.leapfrog_staggered_euler(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, filtercm=True, it=i, filterit=100)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)
    if time_method == 7:
        ### LEAPFROG OPEN BC ###
        unext, vnext, hnext = met.leapfrog_open(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, filtercm=False, it=i, filterit=100)
    if time_method == 8:
        ### LEAPFROG TIMESPLIT ###
        unext, vnext, hnext = met.leapfrog_timesplit(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, nt=100, filtercm=True, it=i, filterit=100)
        unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub[i, :], vb[i, :], hb[i, :], boff, dt)

    #Save iteration to output variables
    u[i, :] = unext
    v[i, :] = vnext
    h[i, :] = hnext

    #Rename variables for next iteration
    if time_method != 1:
        u0 = u1
        v0 = v1
        h0 = h1

    u1 = unext
    v1 = vnext
    h1 = hnext

    #Compute diagnostic variables for analysis
    ke = np.sum(0.5*(u[i, 1:-2]**2 + v[i, 1:-2]**2)*h[i, 1:-2])
    pe = np.sum(0.5*g*(h[i, 1:-2])**2)
    m = np.sum(h[i, 1:-1])
    KE.append(ke)
    PE.append(pe)
    M.append(m)

print('End of integration')

#### SAVE VARIABLES TO NPZ FILE ####
if bc_type == 2:
    u_boundary = ub[:, boff:boff+nx]
    v_boundary = vb[:, boff:boff+nx]
    h_boundary = hb[:, boff:boff+nx]
else:
    u_boundary = np.nan
    v_boundary = np.nan
    h_boundary = np.nan

np.savez(outdir + '/' + fileout, u=u, v=v, h=h, uini=uini, vini=vini, hini=hini, u_boundary=u_boundary, v_boundary=v_boundary, h_boundary=h_boundary, dx=dx, dt=dt, nx=nx, nt=nt, f=f, space_method=space_method, time_method=time_method, ic_type=ic_type, bc_type=bc_type)

#### COMPUTATION OF OTHER DIAGNOSTIC VARIABLES ####
Etot = [sum(x)/(sum(KE)/len(KE)+PE[0]) for x in zip(KE, PE)]
#Etot = [sum(x)/(KE[1]+PE[0]) for x in zip(KE, PE)]
hspec = fft(h[:-1, 1:-1])
hpower = np.real(hspec*hspec.conjugate()/nx)
hpower[hpower == 0] = np.nan
uspec = fft(u[:-1, 1:-1])
upower = np.real(uspec*uspec.conjugate()/nx)
upower[upower == 0] = np.nan

#### GRAPHICS ####
print('Plotting')
hdata = np.ma.masked_invalid(np.log(hpower[:, 0:math.ceil(nx/2)]))
fig, ax = plt.subplots()
pc = ax.pcolormesh(hdata, vmin=-10, vmax=20)
fig.colorbar(pc, ticks=np.arange(-10, 20.01, 5))
ax.set_xlim([0, math.ceil(nx/2)])
ax.set_ylim([0, nt])
ax.set_xlabel('Numero de Onda')
ax.set_ylabel('Cantidad de pasos de tiempo')
ax.set_title('Espectro de H')
fig.savefig(outdir + '/espectro_h.png', transparent=False, dpi=100, bbox_inches='tight')
plt.close(fig)

udata = np.ma.masked_invalid(np.log(upower[:, 0:math.ceil(nx/2)]))
fig, ax = plt.subplots()
pc = ax.pcolormesh(udata, vmin=-10, vmax=20)
fig.colorbar(pc, ticks=np.arange(-10, 20.01, 5))
ax.set_xlim([0, math.ceil(nx/2)])
ax.set_ylim([0, nt])
ax.set_xlabel('Numero de Onda')
ax.set_ylabel('Cantidad de pasos de tiempo')
ax.set_title('Espectro de U')
fig.savefig(outdir + '/espectro_u.png', transparent=False, dpi=100, bbox_inches='tight')
plt.close(fig)

fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, figsize=[6, 9])
for ax, var in zip([ax1, ax2, ax3], [u, v, h]):
    pc = ax.pcolormesh(var[:-1, :])
    fig.colorbar(pc, ax=ax)
    ax.set_ylim([0, nt])
    ax.set_ylabel('Cantidad de pasos de tiempo')
ax1.set_title('Velocidad U (m/s)')
ax2.set_title('Velocidad V (m/s)')
ax3.set_title('Altura H (m)')
ax3.set_xlabel('nx ')
fig.savefig(outdir + '/solucion.png', transparent=False, dpi=100, bbox_inches='tight')
plt.close(fig)

fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True, figsize=[6, 9])
for ax, var in zip([ax1, ax2, ax3, ax4], [KE/(sum(KE)/len(KE)), PE/PE[0], Etot, M]):
#for ax, var in zip([ax1, ax2, ax3, ax4], [KE/KE[1], PE/PE[0], Etot, M]):
    pc = ax.plot(var)
ax2.set_ylim([0.99, 1.01])
ax3.set_ylim([0.99, 1.01])
ax1.set_title('Energia Cinetica')
ax2.set_title('Energia Potencial')
ax3.set_title('Energia Total')
ax4.set_title('Masa')
ax4.set_xlabel('Cantidad de pasos de tiempo')
fig.savefig(outdir + '/conservacion.png', transparent=False, dpi=100, bbox_inches='tight')
plt.close(fig)
