'''
Functions for space derivatives and time integration methods for the shallow water unidimensional model
'''
import numpy as np
import bc

def diffx(var, dx, method):
    '''
    Compute space derivatives according to `method`
    '''
    nx = len(var)
    if method == 1:
        ### 2nd ORDER CENTERED SPACE ###
        var1 = np.empty(nx)*np.nan
        var2 = np.empty(nx)*np.nan
        var1[0:-1] = var[1:]
        var2[1:] = var[0:-1]

        der = (var1-var2)/(2*dx)
        der[0] = (var[1]-var[0])/dx
        der[-1] = (var[-1]-var[-2])/dx

    elif method == 2:
        ### 6th ORDER CENTERED SPACE ###
        der = np.empty(nx)*np.nan

        for i in range(3, nx-3):
            der[i] = (1/dx)*((45/60)*(var[i+1]-var[i-1])
                             -(9/60)*(var[i+2]-var[i-2])
                             +(1/60)*(var[i+3]-var[i-3]))

        der[0] = (var[1]-var[0])/dx
        der[1] = (var[2]-var[0])/(2*dx)
        der[2] = (var[3]-var[1])/(2*dx)
        der[-1] = (var[-1]-var[-2])/dx
        der[-2] = (var[-1]-var[-3])/(2*dx)
        der[-3] = (var[-2]-var[-4])/(2*dx)

    return der

def euler(u, v, h, dx, dt, space_method, f, g):

    #Right side terms
    fU = f*u
    fV = f*v
    UH = u*h

    #Space derivatives
    Ux = diffx(u, dx, space_method)
    Vx = diffx(v, dx, space_method)
    Hx = diffx(h, dx, space_method)
    UHx = diffx(UH, dx, space_method)

    #Time derivatives
    Ut = -u*Ux - g*Hx + fV
    Vt = -u*Vx -fU
    Ht = -UHx

    #Time integration
    unext = u + dt*Ut
    vnext = v + dt*Vt
    hnext = h + dt*Ht

    return unext, vnext, hnext

def euler_lineal(u, v, h, dx, dt, space_method, f, g):
    #Right side terms
    fU = f*u
    fV = f*v

    #Space derivatives
    Ux = diffx(u, dx, space_method)
    Vx = diffx(v, dx, space_method)
    Hx = diffx(h, dx, space_method)

    #Time derivatives
    Umedio = np.mean(u)
    Hmedio = np.mean(h)

    Ut = -Umedio*Ux - g*Hx + fV
    Vt = -Umedio*Vx -fU
    Ht = -Hmedio*Ux - Umedio*Hx

    #Time integration
    unext = u + dt*Ut
    vnext = v + dt*Vt
    hnext = h + dt*Ht

    return unext, vnext, hnext

def leapfrog(uprev, vprev, hprev, u, v, h, dx, dt, space_method, f, g, filtercm=False, it=None, filterit=100):

    #Right side terms
    fU = f*u
    fV = f*v
    UH = u*h

    #Space derivatives
    Ux = diffx(u, dx, space_method)
    Vx = diffx(v, dx, space_method)
    Hx = diffx(h, dx, space_method)
    UHx = diffx(UH, dx, space_method)

    #Time derivatives
    Ut = -u*Ux - g*Hx + fV
    Vt = -u*Vx -fU
    Ht = -UHx

    #Time integration
    unext = uprev + 2*dt*Ut
    vnext = vprev + 2*dt*Vt
    hnext = hprev + 2*dt*Ht

    #Filter the computational mode solution every `filterit` time-step
    if filtercm and it % filterit == 0 :
        unext = (unext+u)/2
        vnext = (vnext+v)/2
        hnext = (hnext+h)/2

    return unext, vnext, hnext

def leapfrog_lineal(uprev, vprev, hprev, u, v, h, dx, dt, space_method, f, g, filtercm=False, it=None, filterit=100):

    #Right side terms
    fU = f*u
    fV = f*v

    #Space derivatives
    Ux = diffx(u, dx, space_method)
    Vx = diffx(v, dx, space_method)
    Hx = diffx(h, dx, space_method)

    #Time derivatives
    Umedio = np.mean(u)
    Hmedio = np.mean(h)

    Ut = -Umedio*Ux - g*Hx + fV
    Vt = -Umedio*Vx -fU
    Ht = -Hmedio*Ux - Umedio*Hx

    #Time integration
    unext = uprev + 2*dt*Ut
    vnext = vprev + 2*dt*Vt
    hnext = hprev + 2*dt*Ht

    #Filter the computational mode solution every `filterit` time-step
    if filtercm and it % filterit == 0 :
        unext = (unext+u)/2
        vnext = (vnext+v)/2
        hnext = (hnext+h)/2

    return unext, vnext, hnext

def leapfrog_euler(uprev,vprev,hprev,u,v,h,dx,dt,space_method, f, g, filtercm=False, it=None, filterit=100):
    nx=np.size(uprev)

    #Termino de Coriolis
    fuh = f*u*h
    fvh = f*v*h

    #Cantidad de movimiento
    uh = h*u
    hh = h*h/2

    #calculo las derivadas espaciales.
    uuhx = np.zeros(nx)
    vuhx = np.zeros(nx)
    for i in range(1, nx-1):

        uuhx[i] = ((uh[i+1]+uh[i])*(u[i+1]+u[i]) - (uh[i-1]+uh[i])*(u[i-1]+u[i]))/(4*dx)
        vuhx[i] = ((uh[i+1]+uh[i])*(v[i+1]+v[i]) - (uh[i-1]+uh[i])*(v[i-1]+v[i]))/(4*dx)

    uuhx[0] = ((uh[1]+uh[0])*(u[1]+u[0]) - (uh[-3]+uh[-2])*(u[-3]+u[-2]))/(4*dx)
    vuhx[0] = ((uh[1]+uh[0])*(v[1]+v[0]) - (uh[-3]+uh[-2])*(v[-3]+v[-2]))/(4*dx)

    uuhx[-1] = ((uh[2]+uh[-1])*(u[2]+u[-1]) - (uh[-2]+uh[-1])*(u[-2]+u[-1]))/(4*dx)
    vuhx[-1] = ((uh[2]+uh[-1])*(v[2]+v[-1]) - (uh[-2]+uh[-1])*(v[-2]+v[-1]))/(4*dx)

    hhx = diffx(hh, dx, space_method)
    uhx = diffx(uh, dx, space_method)

    uht = -uuhx - g*hhx + fvh
    ht = -uhx
    vht = -vuhx - fuh

    hnext = hprev + 2*dt*ht
    unext = uprev*hprev + 2*dt*uht
    vnext = vprev*hprev + 2*dt*vht

    #Recupero U y V dividiendo nuevamente por h.
    unext = unext/hnext
    vnext = vnext/hnext

    #Filtrado del modo computacional cada filterit pasos de tiempo.
    if filtercm and it % filterit == 0:
        unext = (unext+u)/2
        vnext = (vnext+v)/2
        hnext = (hnext+h)/2

    return unext, vnext, hnext

def leapfrog_staggered_euler(uprev, vprev, hprev, u, v, h, dx, dt, space_method, f, g, filtercm=False, it=None, filterit=100):
    '''
    (U)t + div(uU)= -fi*(fi)x
    (fi)t+div(U)=0
    U=uh cantidad de movimiento.

    Staggered
     ------0------x------0------x-------0------x------0------x
           hi-1         hi            hi+1           hi+2
                 ui-1/2       ui+1/2        ui+1+1/2

    Las reticulas staggered son eficientes en el tratamiento de las ondas de
    gravedad.
    ESTA FUNCION SOLO SE PUEDE UTILIZAR CON CONDICIONES DE BORDE CICLICAS!!!
    '''
    nx = np.size(uprev)

    umedio = np.zeros(nx)
    vmedio = np.zeros(nx)
    hmedio = np.zeros(nx)
    hmedioprev = np.zeros(nx)
    hmedionext = np.zeros(nx)

    #Variables de movimiento interpolados en los puntos de masa.
    for i in range(1, nx):
        umedio[i] = (u[i-1]+u[i])/2
        vmedio[i] = (v[i-1]+v[i])/2

    #Variables de masa interpoladas en los puntos de velocidad.
    for i in range(0, nx-1):
        hmedio[i] = (h[i]+h[i+1])/2
        hmedioprev[i] = (hprev[i]+hprev[i+1])/2

    umedio[0] = (u[-3]+u[-2])/2
    vmedio[0] = (v[-3]+v[-2])/2

    hmedio[-1] = (h[-1]+h[2])/2
    hmedioprev[-1] = (hprev[-1]+hprev[2])/2

    fuh = f*u*hmedio
    fvh = f*v*hmedio

    hu = hmedio*u
    uuh = hmedio*u*u
    vuh = hmedio*v*u
    hh = 0.5*np.power(hmedio, 2)

    uuhx = diffx(uuh, dx, space_method)
    vuhx = diffx(vuh, dx, space_method)
    hhx = diffx(hh, dx, space_method)

    hux = np.zeros(nx)
    for i in range(1, nx):
        hux[i] = (hu[i]-hu[i-1])/dx
    hux[0] = (hu[0]-hu[-3])/dx

    uht = -uuhx - g*hhx + fvh
    ht = -hux
    vht = -vuhx - fuh

    unext = uprev*hmedioprev+2*dt*uht
    vnext = vprev*hmedioprev+2*dt*vht
    hnext = hprev+2*dt*ht

    for i in range(0, nx-1):
        hmedionext[i] = (hnext[i]+hnext[i+1])/2

    hmedionext[-1] = (hnext[-1]+hnext[2])/2
    unext = unext/hmedionext
    vnext = vnext/hmedionext

    #Filtrado del modo computacional cada filterit pasos de tiempo.
    if filtercm and it % filterit == 0:
        unext = (unext+u)/2
        vnext = (vnext+v)/2
        hnext = (hnext+h)/2

    return unext, vnext, hnext

def leapfrog_open(uprev, vprev, hprev, u, v, h, dx, dt, space_method, f, g, filtercm=False, it=None, filterit=100):

    unext, vnext, hnext = leapfrog(uprev, vprev, hprev, u, v, h, dx, dt, space_method, f, g, filtercm=filtercm, it=it, filterit=filterit)

    #Update boundary conditions
    Ux = diffx(u, dx, space_method)
    Hx = diffx(h, dx, space_method)
    #Velocidad de propagacion de las onda de gravedad
    cg = np.real(np.sqrt(np.nanmean(h)*g))

    #Ecuacion de movimiento
    #Borde oeste
    Utw = -np.min([u[0]-cg, 0])*Ux[1]
    unext[0] = uprev[0] + 2*dt*Utw
    #Borde este
    Ute = -np.max([u[-1]+cg, 0])*Ux[-2]
    unext[-1] = uprev[-1] + 2*dt*Ute
    #Ecuacion para h
    #Borde oeste
    Htw = -np.min([u[0], 0])*Hx[0] - h[0]*Ux[0]
    hnext [0] = hprev[0] + 2*dt*Htw
    #Borde este
    Hte = -np.max([u[-1], 0])*Hx[-1] - h[-1]*Ux[-1]
    hnext[-1] = hprev[-1] + 2*dt*Hte

    return unext, vnext, hnext

def leapfrog_timesplit(uprev, vprev, hprev, u, v, h, dx, dt, space_method, f, g, nt=None, filtercm=False, it=None, filterit=100):
    '''
    Calcula los terminos que tienen que ver con las ondas de gravedad en pasos de tiempos mas cortos y la adveccion en pasos de tiempo mas largos.
    nt: cuantos pasos pequenos mas hago por cada paso grande
    '''
    #Right side terms
    fU = f*u
    fV = f*v

    #Space derivatives
    Ux = diffx(u, dx, space_method)
    Vx = diffx(v, dx, space_method)
    Hx = diffx(h, dx, space_method)

    #Time derivatives
    #Compute "slow" terms
    Ut = -u*Ux + fV
    Vt = -u*Vx - fU
    Ht = -u*Hx

    utmp = 2*dt*Ut
    vtmp = 2*dt*Vt
    htmp = 2*dt*Ht

    #Compute "fast" terms
    i = 0
    while i < nt:
        Ux = diffx(uprev, dx, space_method)
        Hx = diffx(hprev, dx, space_method)

        uprev = uprev - g*Hx*dt*2/nt + utmp/nt
        vprev = vprev + vtmp/nt
        hprev = hprev - hprev*Ux*dt*2/nt + htmp/nt

        i += 1

    unext = uprev
    vnext = vprev
    hnext = hprev

    #Filtrado del modo computacional cada filterit pasos de tiempo.
    if filtercm and it % filterit == 0:
        unext = (unext+u)/2
        vnext = (vnext+v)/2
        hnext = (hnext+h)/2

    return unext, vnext, hnext

def rk3(u, v, h, dx, dt, space_method, f, g, bc_type, ub, vb, hb, boff, it=None):
    '''
    El metodo RK3 utilizado por el WRF se puede pensar como un paso de Euler y 2 pasos de Leapfrog. El costo computacional del esquema es equivalente a dichos calculos.
    '''
    #Euler
    u1, v1, h1 = leapfrog(u, v, h, u, v, h, dx, dt/6, space_method, f, g, filtercm=True, it=it, filterit=100)
    u1, v1, h1 = bc.boundary(u1, v1, h1, bc_type, ub, vb, hb, boff, dt)

    #Leapfrog
    u2, v2, h2 = leapfrog(u, v, h, u1, v1, h1, dx, dt/4, space_method, f, g, filtercm=True, it=it, filterit=100)
    u2, v2, h2 = bc.boundary(u2, v2, h2, bc_type, ub, vb, hb, boff, dt)
    unext, vnext, hnext = leapfrog(u, v, h, u2, v2, h2, dx, dt/2, space_method, f, g, filtercm=True, it=it, filterit=100)

    return unext, vnext, hnext
