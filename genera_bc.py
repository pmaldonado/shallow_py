'''
SHALLOW WATER UNIDIMENSIONAL MODEL

En este caso vamos a hacer una simulacion que va a ser 50% mas grande que
la simulacion normal (usualmente nx=1000). Para que sirva como condicion
de contorno a la simulacion mas pequeña. Es importante que el valor de altura
de base coincida en ambas simulaciones al igual que el dx y el dt. Esta
simulacion usa condiciones de borde ciclicas como un modelo global.

Equations:
 1) Ut = -U*Ux - gHx + fV
 2) Vt = -UVx - fU
 3) Ht = -(UH)x
'''
import os
import numpy as np
import bc
import methods as met

#### SET UP PARAMETERS AND VARIABLES ####
outdir = './bc'
if not os.path.exists(outdir):
    os.makedirs(outdir)

nx = 500  #Number of points in the x-direction
nt = 2000 #Number of times to integrate the model
dx = 500  #Space between nodes (in meters)
dt = 10   #Time interval between integrations (in seconds)
gwidth = 10000 #Gauss width in m

bc_type = 1 #Boundary condition type
space_method = 1 #Space integration method
time_method = 2  #Time integration method

f = 7.29e-5   #Coriolis parameter
g = 9.8       #Gravitiy
hs = 0.0      #Bottom height (not used for now)
#------------------------------------------------------------------------------#
#### ALLOCATE VARIABLES ####
u = np.empty((nt, nx))*np.nan
v = np.empty((nt, nx))*np.nan
h = np.empty((nt, nx))*np.nan

#### INITIAL AND (INITIAL) BOUNDARY CONDITIONS####
#Set up initial conditions
x = np.arange(0,nx)*dx
uini = np.zeros(nx)
vini = np.zeros(nx)

hini = 50 + 5*np.exp(-(x**2)/(gwidth**2))
hini = hini + 5*np.exp(-((x-x[-1])**2)/(gwidth**2))

#Set up boundary conditions
boff = np.nan
ub, vb, hb = np.nan, np.nan, np.nan

u0 = uini
v0 = vini
h0 = hini

#Save initial time to output variables
u[0, :] = u0
v[0, :] = v0
h[0, :] = h0

#### BEGIN INTEGRATION ####
#First step: EULER
u1, v1, h1 = met.euler(u0, v0, h0, dx, dt, space_method, f, g)
u1, v1, h1 = bc.boundary(u1, v1, h1, bc_type, ub, vb, hb, boff, dt)

#Save time step to output variables
u[1, :] = u1
v[1, :] = v1
h[1, :] = h1

#Rest of integration steps: LEAPFROG
for i in range(2, nt):
    unext, vnext, hnext = met.leapfrog(u0, v0, h0, u1, v1, h1, dx, dt, space_method, f, g, filtercm=True, it=i, filterit=100)
    unext, vnext, hnext = bc.boundary(unext, vnext, hnext, bc_type, ub, vb, hb, boff, dt)

    #Save iteration to output variables
    u[i, :] = unext
    v[i, :] = vnext
    h[i, :] = hnext

    #Rename variables for next iteration
    u0 = u1
    v0 = v1
    h0 = h1

    u1 = unext
    v1 = vnext
    h1 = hnext

print('End of integration')

#### SAVE VARIABLES TO NPZ FILE ####
np.savez(outdir + '/bc.npz', ub=u, vb=v, hb=h, uini=uini, vini=vini, hini=hini, dx=dx, dt=dt, nx=nx, nt=nt, f=f, space_method=space_method, bc_type=bc_type)

'''
Vamos a generar una condicion de borde alternativa que solo esta
disponible cada stepbc pasos de tiempo del modelo. La idea es simular
condiciones de borde generadas a partir de salidas espaciadas un periodo
largo en el tiempo (como por ejemplo 3 horas o 6 horas como normalmente se
hace con los modelos regionales).
Este experimento convierte las condiciones de borde originales a
codiciones de borde interpoladas en el tiempo usando una interpolacion
lineal.
'''
stepbc = 100
utmp = u
vtmp = v
htmp = h
for i in range(int(nt/stepbc)):
    for it in range(stepbc):
        utmp[i*stepbc+it, :] = (it+1)*(utmp[(i+1)*stepbc-1, :]-utmp[i*stepbc, :])/(stepbc-1) + utmp[i*stepbc, :]
        vtmp[i*stepbc+it, :] = (it+1)*(vtmp[(i+1)*stepbc-1, :]-vtmp[i*stepbc, :])/(stepbc-1) + vtmp[i*stepbc, :]
        htmp[i*stepbc+it, :] = (it+1)*(htmp[(i+1)*stepbc-1, :]-htmp[i*stepbc, :])/(stepbc-1) + htmp[i*stepbc, :]

print(utmp.shape)

#### SAVE VARIABLES TO NPZ FILE ####
np.savez(outdir + '/bc_interp.npz', ub=utmp, vb=vtmp, hb=htmp, uini=uini, vini=vini, hini=hini, dx=dx, dt=dt, nx=nx, nt=nt, f=f, space_method=space_method, bc_type=bc_type)
