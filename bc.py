'''
Function for boundary conditions for the shallow water unidimensional model
'''
def boundary(u, v, h, bc_type, ub, vb, hb, boff, dt):
    '''
    Update boundary condition according to chosen method
    '''
    import sys
    import numpy as np

    nx = len(u)

    if bc_type == 1:
        ### CYCLIC ###
        u[0] = u[-2]
        v[0] = v[-2]
        h[0] = h[-2]

        u[-1] = u[1]
        v[-1] = v[1]
        h[-1] = h[1]

    elif bc_type == 2:
        ### SPECIFIED ###
        relax = True
        relax_zone = 4 #Number of points from the boundaries of the regional domain that will be relaxed towards the global domain

        u[0] = ub[boff]
        v[0] = vb[boff]
        h[0] = hb[boff]

        u[-1] = ub[boff+nx]
        v[-1] = vb[boff+nx]
        h[-1] = hb[boff+nx]

        if relax:
            relax_time = dt*np.exp(np.arange(0, relax_zone))
            for i in range(0, relax_zone):
                #var = var + dt*(varb-var)/tau
                u[i+1] = u[i+1] + dt*(ub[boff+i+1]-u[i+1])/relax_time[i]
                v[i+1] = v[i+1] + dt*(vb[boff+i+1]-v[i+1])/relax_time[i]
                h[i+1] = h[i+1] + dt*(hb[boff+i+1]-h[i+1])/relax_time[i]

                u[-i-2] = u[-i-2] + dt*(ub[boff+nx-i-2]-u[-i-2])/relax_time[i]
                v[-i-2] = v[-i-2] + dt*(vb[boff+nx-i-2]-v[-i-2])/relax_time[i]
                h[-i-2] = h[-i-2] + dt*(hb[boff+nx-i-2]-h[-i-2])/relax_time[i]

    elif bc_type == 3:
        ### RIGID ###
        u[0] = 0
        u[-1] = 0

    elif bc_type == 4:
        ### ANTI-SYMETRIC ###
        u[0] = -u[1]
        u[-1] = -u[-2]

        h[0] = h[2]
        h[-1] = h[-2]

    elif bc_type == 5:
        ### FIXED ###
        u[0] = 0
        u[-1] = 0

        h[0] = 51
        h[-1] = 51

    else:
        sys.exit('Not a valid bc_type option')

    return u, v, h
